﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace VamonosDeCurvas.Models.Database.DbObject
{
    public class UsuarioDbObject : DbObject
    {
        public Guid id { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        public UsuarioDbObject(DataRow row) : base(row)
        {
            this.id = (Guid)row["id"];
            this.email = (string)row["email"];
            this.password = (string)row["password"];
        }
    }
}
