﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace VamonosDeCurvas.Models.Database.DbObject
{
    public class FotosDbObject : DbObject
    {
        public Guid id { get; set; }
        public string titulo { get; set; }
        public DateTime fecha { get; set; }
        public string foto { get; set; }
        public string userEmail { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }

        public string visibleDate { get; set; }

        public FotosDbObject(DataRow row) : base(row)
        {
            this.id = (Guid)row["id"];
            this.titulo = ((string)row["titulo"]).Trim();
            this.fecha = (DateTime)row["fecha"];

            if (row.Table.Columns.Contains("foto"))
                this.foto = (string)row["foto"];

            var position = (string)row["posicion"];

            this.latitude = position.Split(";", StringSplitOptions.RemoveEmptyEntries).First();
            this.longitude = position.Split(";", StringSplitOptions.RemoveEmptyEntries).Last();

            this.userEmail = ((string)row["email"]).Trim(); ;
        }
    }
}