﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace VamonosDeCurvas.Models.Database.DbObject
{
    public class RutasDbObject : DbObject
    {
     

        public Guid id { get; set; }
        public string horas { get; set; }
        public string kilometros { get; set; }
        public string personalizar { get; set; }
        public string describe { get; set; }

        public RutasDbObject(DataRow row) : base(row)
        {
            this.id = (Guid)row["id"];
            this.personalizar = ((string)row["personalizar"]);
            this.horas = ((string)row["horas"]).Trim();
            this.kilometros = ((string)row["kilometros"]);
            
        }
    }
}
