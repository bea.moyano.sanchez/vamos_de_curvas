﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using VamonosDeCurvas.Models.Database.DbObject;

namespace VamonosDeCurvas.Models.Database.DbManagers
{
    public class FotosDbManager : DbManager
    {
        public FotosDbManager(string _connectionString) : base(_connectionString)
        {
        }

        public FotosDbObject Insertar(string titulo, DateTime fecha, string foto, Guid userId, string posicion)
        {
            try
            {
                var database = this.CreateDatabase();

                using (var dbCommand = database.GetStoredProcCommand("Fotos_Insert"))
                {
                    database.AddInParameter(dbCommand, "@titulo", System.Data.DbType.String, titulo);
                    database.AddInParameter(dbCommand, "@fecha", System.Data.DbType.Date, fecha);
                    database.AddInParameter(dbCommand, "@foto", System.Data.DbType.String, foto);
                    database.AddInParameter(dbCommand, "@idUsuario", System.Data.DbType.Guid, userId);
                    database.AddInParameter(dbCommand, "@posicion", System.Data.DbType.String, posicion);

                    using (var dataSet = database.ExecuteDataSet(dbCommand))
                    {
                        if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                        {
                            return new FotosDbObject(dataSet.Tables[0].Rows[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return null;
        }

        public List<FotosDbObject> SelecionarTodasLasFotos()
        {
            try
            {
                var fotos = new List<FotosDbObject>();

                var database = this.CreateDatabase();

                using (var dbCommand = database.GetStoredProcCommand("Fotos_Select"))
                {
                    using (var dataSet = database.ExecuteDataSet(dbCommand))
                    {
                        if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row in dataSet.Tables[0].Rows)
                                fotos.Add(new FotosDbObject(row));
                        }
                    }
                }

                return fotos;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return null;
        }

        public FotosDbObject SeleccionarPorId(Guid id)
        {
            try
            {
                var database = this.CreateDatabase();

                using (var dbCommand = database.GetStoredProcCommand("Fotos_SelectById"))
                {
                    database.AddInParameter(dbCommand, "@id", System.Data.DbType.Guid, id);

                    using (var dataSet = database.ExecuteDataSet(dbCommand))
                    {
                        if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                        {
                            return new FotosDbObject(dataSet.Tables[0].Rows[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return null;
        }

        public List<FotosDbObject> SelecionarFotosAleatorias()
        {
            try
            {
                var fotos = new List<FotosDbObject>();

                var database = this.CreateDatabase();

                using (var dbCommand = database.GetStoredProcCommand("Fotos_Select_Home"))
                {
                    using (var dataSet = database.ExecuteDataSet(dbCommand))
                    {
                        if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row in dataSet.Tables[0].Rows)
                                fotos.Add(new FotosDbObject(row));
                        }
                    }
                }

                return fotos;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return null;
        }
    }
}
