﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using VamonosDeCurvas.Models.Database.DbObject;

namespace VamonosDeCurvas.Models.Database.DbManagers
{
    public class RutasDbManager : DbManager
    {
        public RutasDbManager(string _connectionString) : base(_connectionString)
        {
        }

        public RutasDbObject Insertar(string horas, string kilometros, string personalizar, string describe)
        {
            try
            {
                var database = this.CreateDatabase();

                using (var dbCommand = database.GetStoredProcCommand("Rutas_Insert"))
                {
                    database.AddInParameter(dbCommand, "@horas", System.Data.DbType.String, horas);
                    database.AddInParameter(dbCommand, "@kilometros", System.Data.DbType.String, kilometros);
                    database.AddInParameter(dbCommand, "@personalizar", System.Data.DbType.String, personalizar);
                    database.AddInParameter(dbCommand, "@describe", System.Data.DbType.Guid, describe);

                    using (var dataSet = database.ExecuteDataSet(dbCommand))
                    {
                        if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                        {
                            return new RutasDbObject(dataSet.Tables[0].Rows[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return null;
        }

        public List<RutasDbObject> SeleccionarTodasLasRutas()
        {
            try
            {
                var rutas = new List<RutasDbObject>();

                var database = this.CreateDatabase();

                using (var dbCommand = database.GetStoredProcCommand("Rutas_select"))
                {
                    using (var dataSet = database.ExecuteDataSet(dbCommand))
                    {
                        if(dataSet! != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow row in dataSet.Tables[0].Rows)
                                rutas.Add(new RutasDbObject(row));
                        }
                    }
                }

                return rutas;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return null;
        }
       
    }
}
