﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace VamonosDeCurvas.Models.Database.DbManagers
{
    public class DbManager
    {
        private string connectionString;

        public DbManager(string _connectionString)
        {
            this.connectionString = _connectionString;
        }

        public SqlDatabase CreateDatabase()
        {
            return new SqlDatabase(connectionString);
        }

        public void HandleException(Exception ex)
        {
            try
            {
                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Vamonos de Curvas";
                    eventLog.WriteEntry(ex.Message, EventLogEntryType.Error);
                }
            }
            catch (Exception ex2)
            {

            }
        }

    }
}
