﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using VamonosDeCurvas.Models.Database.DbObject;

namespace VamonosDeCurvas.Models.Database.DbManagers
{
    public class UsuarioDbManager : DbManager
    {     
        public UsuarioDbManager(string _connectionString) : base(_connectionString)
        {
        }

        public UsuarioDbObject Insertar(string email, string password)
        {
            try
            {
                var database = this.CreateDatabase();

                using (var dbCommand = database.GetStoredProcCommand("Usuarios_Insert"))
                {
                    database.AddInParameter(dbCommand, "@email", System.Data.DbType.String, email);
                    database.AddInParameter(dbCommand, "@password", System.Data.DbType.String, password);

                    using (var dataSet = database.ExecuteDataSet(dbCommand))
                    {
                        if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                        {
                            return new UsuarioDbObject(dataSet.Tables[0].Rows[0]);
                        }
                    }
                }                
            }
            catch(Exception ex)
            {
                HandleException(ex);
            }

            return null;
        }

        public UsuarioDbObject SeleccionarPorEmail(string email)
        {
            try
            {
                var database = this.CreateDatabase();

                using (var dbCommand = database.GetStoredProcCommand("Usuarios_Select"))
                {
                    database.AddInParameter(dbCommand, "@email", System.Data.DbType.String, email);

                    using (var dataSet = database.ExecuteDataSet(dbCommand))
                    {
                        if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                        {
                            return new UsuarioDbObject(dataSet.Tables[0].Rows[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return null;
        }

        public UsuarioDbObject SeleccionarPorId(Guid id)
        {
            try
            {
                var database = this.CreateDatabase();

                using (var dbCommand = database.GetStoredProcCommand("Usuarios_SelectById"))
                {
                    database.AddInParameter(dbCommand, "@id", System.Data.DbType.Guid, id);

                    using (var dataSet = database.ExecuteDataSet(dbCommand))
                    {
                        if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                        {
                            return new UsuarioDbObject(dataSet.Tables[0].Rows[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return null;
        }
        public UsuarioDbObject SeleccionarPorTitulo(string titulo)
        {
            try
            {
                var database = this.CreateDatabase();

                using (var dbCommand = database.GetStoredProcCommand("Usuarios_Buscar"))
                {
                    database.AddInParameter(dbCommand, "@titulo", System.Data.DbType.String,titulo);

                    using (var dataSet = database.ExecuteDataSet(dbCommand))
                    {
                        if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                        {
                            return new UsuarioDbObject(dataSet.Tables[0].Rows[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return null;
        }
    }
}
