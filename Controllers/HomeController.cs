﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VamonosDeCurvas.Models;
using VamonosDeCurvas.Models.Database.DbManagers;
using VamonosDeCurvas.Models.Database.DbObject;

namespace VamonosDeCurvas.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(IConfiguration configuration, ILogger<HomeController> logger) : base(configuration)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            FotosDbManager fotosDbManager = new FotosDbManager(this.dbConnectionString);

            List<FotosDbObject> fotos = fotosDbManager.SelecionarFotosAleatorias();

            return View("/Views/Home/Index.cshtml", fotos);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Logout()
        {
            Response.Cookies.Delete("uid");
            ViewBag.UserLogin = false;
            return Index();
        }

        public IActionResult Login(string email, string password)
        {
            UsuarioDbManager usuarioDbManager = new UsuarioDbManager(this.dbConnectionString);

            UsuarioDbObject usuarioDbObject;

            // 1. Comprobar si ya existe un usuario con ese email en la base de datos
            usuarioDbObject = usuarioDbManager.SeleccionarPorEmail(email);

            //  1.1 Si existe comprobamos que la password sea la misma
            if (usuarioDbObject != null)
            {
                //      Si la password es la misma, le dejamos pasar, sino le mostramos el error
                if(usuarioDbObject.password == password)
                {
                    Response.Cookies.Append("uid", usuarioDbObject.id.ToString());
                    ViewBag.UserEmail = email;
                    ViewBag.UserLogin = true;
                }
                else
                {
                    // error
                    ViewBag.Login_Error =true;
                    ViewBag.Login_Message = "La contraseña no es correcta";
                    ViewBag.email = email;
                    ViewBag.password = password;
                }
            }            
            else {
                //  1.2 Si no existe, lo creamos con esa password                
                usuarioDbObject = usuarioDbManager.Insertar(email, password);

                //      Si se ha creado bien, le dejamos pasar
                if (usuarioDbObject != null)
                {
                    Response.Cookies.Append("uid", usuarioDbObject.id.ToString());
                    ViewBag.UserLogin = true;
                    ViewBag.UserEmail = email;
                }
                else
                {
                    // error
                    ViewBag.Login_Error = true;
                    ViewBag.Login_Message = "Error creando el usuario";
                    ViewBag.email = email;
                    ViewBag.password = password;
                }
            }

            return Index();
        }

    }
}
