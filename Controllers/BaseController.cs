﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using VamonosDeCurvas.Models;
using VamonosDeCurvas.Models.Database.DbManagers;
using VamonosDeCurvas.Models.Database.DbObject;

namespace VamonosDeCurvas.Controllers
{
    public class BaseController : Controller
    {
        public UsuarioDbObject usuario;
        public string dbConnectionString;

        public BaseController(IConfiguration configuration)
        {
            this.dbConnectionString = configuration["DatabaseConnectionString"];
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            // si tenemos la cookie del login del usuario
            if (Request.Cookies.ContainsKey("uid"))
            {
                // instanciamos el manager
                UsuarioDbManager usuarioDbManager = new UsuarioDbManager(this.dbConnectionString);

                // comprobamos que el codigo del usuario tiene el formato correcto
                Guid userId;
                if (Guid.TryParse(Request.Cookies["uid"], out userId))
                {
                    // recuperamos el usuario por su ID
                    UsuarioDbObject usuarioDbObject = usuarioDbManager.SeleccionarPorId(userId);

                    // si existe un usuario con ese ID
                    if (usuarioDbObject != null)
                    {
                        // nos guardamos el usuario
                        this.usuario = usuarioDbObject;

                        // ponemos los datos
                        ViewBag.UserLogin = true;
                        ViewBag.UserEmail = usuarioDbObject.email;
                    }
                }
            }
            else
            {
                ViewBag.UserLogin = false;
            }

            base.OnActionExecuting(context);
        }

    }
}
