﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VamonosDeCurvas.Models;
using VamonosDeCurvas.Models.Database.DbObject;
using VamonosDeCurvas.Models.Database.DbManagers;

namespace VamonosDeCurvas.Controllers
{
    public class RutasController : BaseController
    {
        public IActionResult Index()
        {
            return View("/Views/Rutas/Index.cshtml");
        }

        public IActionResult AnadeTuRuta()
        {
            return View();
        }
    }
}
