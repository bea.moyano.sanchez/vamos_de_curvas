﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VamonosDeCurvas.Models;
using VamonosDeCurvas.Models.Database.DbObject;
using VamonosDeCurvas.Models.Database.DbManagers;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace VamonosDeCurvas.Controllers
{
    public class FotosController : BaseController
    {
        public FotosController(IConfiguration configuration) : base(configuration)
        {
        }

        public IActionResult Index()
        {            
            return View("/Views/Fotos/Index.cshtml");
        }

        public IActionResult GetFotos()
        {
            FotosDbManager fotosDbManager = new FotosDbManager(this.dbConnectionString);

            List<FotosDbObject> fotos = fotosDbManager.SelecionarTodasLasFotos();

            foreach(var foto in fotos)
            {
                foto.foto = Url.Action("GetPhotoDetail", "Fotos", new { id = foto.id });
                foto.visibleDate = foto.fecha.ToShortDateString();
            }

            return Ok(fotos);
        }

        public IActionResult GetPhotoDetail(Guid id)
        {            
            FotosDbManager fotosDbManager = new FotosDbManager(this.dbConnectionString);
            var foto = fotosDbManager.SeleccionarPorId(id);

            var bytes = Convert.FromBase64String(foto.foto);
            return File(bytes, "image/jpeg");
        }

        public async Task<IActionResult> Fotos_Insert(string title, DateTime date, IFormFile photo, string position)
        {            
            FotosDbManager fotosDbManager = new FotosDbManager(this.dbConnectionString);

            if(photo.Length > (1024*2000))
            {
                ViewBag.Error = "La foto no puede superar los 2MB";
            }
            else
            {
                MemoryStream memoryStream = new MemoryStream();
                photo.CopyTo(memoryStream);
                string base64Image = Convert.ToBase64String(memoryStream.GetBuffer());

                fotosDbManager.Insertar(title, date, base64Image, this.usuario.id, position);
            }

            return Index();
        }

    }
}
